SITEDIR := `guile-config info sitedir`

all:
	@echo "Just type \"sudo make install\""

install:
	mkdir -p $(SITEDIR)
	cp -fr src/suru $(SITEDIR)

uninstall:
	rm -r $(SITEDIR)/suru
