;; Suru - An immutable database written on top of sqlite

(define-module (suru entity-field)
  #:export (entity-field?))

(use-modules (ice-9 match))

(define entity-field? '())

(let ((data-types '(string int float blob))
      (specifiers '(primary auto-increment !null)))
  (define (data-type? val)
    (if (member val data-types)
	#t #f))
  (define (specifier? val)
    (if (member val specifiers)
	#t #f))
  (define -entity-field? (match-lambda
			  (((? specifier? specifier)...
			    (? data-type? data-type) value)
			   #t)
			  (else #f)))
  (set! entity-field? -entity-field?))
