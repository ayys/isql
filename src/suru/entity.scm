;; Suru - An immutable database written on top of sqlite

(define-module (suru entity)
  #:export (entity?
	    entity-to-list
	    add-field-to-entity
	    get-fields-of-entity
	    field-in-entity?
	    ))

(use-modules (ice-9 match))
(use-modules (suru entity-field))

(define entity-to-list
  (match-lambda
    (('entity name (? entity-field? entity-field) ...)
     (list 'entity name entity-field))
    (else #f)))

(define (entity? ent)
  (if (entity-to-list ent)
      #t #f))

(define (add-field-to-entity entity field)
  (if (and (entity? entity) (entity-field? field))
      (append entity (list field))
      #f))

(define (get-fields-of-entity entity)
  (if (entity? entity)
      (caddr (entity-to-list entity))
      #f))

(define (field-in-entity? entity field-name)
  (if (entity? entity)
      (if (member field-name (map (lambda (field) (car (last-pair field))) (get-fields-of-entity entity)))
	  #t
	  #f)
      #f))

;; (entity? '(entity student (string name) (str id)))
;; (entity? '(entity student (string name) (string id)))
;;

(define student-entity '(entity student
				(primary auto-increment int id)
				(string name)
				(int age)
				(float marks)))

(define (get-primary-fields entity)
  (if (entity? entity)
      (filter (lambda (field) (member 'primary field))
	      (get-fields-of-entity entity))
      #f))
