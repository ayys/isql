# SURU

Suru is a DSL that compiles to SQL. It provides an easier syntax for creation and management of relational databases. 

An example of what I expect to have

```
Table students {
    primary unique int id;
    string name;
    College(id) college;
    Address(id) address;
    int age;
}

Table PrimaryStudents extends Students {
    int grade;
}

Table College {
    primary unique int id;
    string name;
    string location;
}

Table Address {
    primary unique int id;
    string street_name;
    string city_name;
    string country_name;
}
```
This will compile to the necessary SQL code to create tables, implement relationships.

  * [ ] Support for class namespaces
  * [ ] Support for Data structures like lists, hash-maps and sets
  * [ ] One-to-one relationships between classes
  * [ ] one-to-many relationships between classes
  * [ ] Handle constraints
  * [ ] Simple I/O library to read and write from DSV file to database.
  * [ ] Loops for writing large datasets into the DB


Some guiding principles of the project

  * Everything must compile down to SQL
  * SQL must be valid for sqlite
  * Suru will never be Turing complete
  * Suru will have a C-Like syntax
  * Suru will try to be orthogonal
